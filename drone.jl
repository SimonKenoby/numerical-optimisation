using JuMP
using PyPlot
using Mosek
include("data.jl")
include("sol.jl")

function Q1(instanceName::String)
 	m = Model(solver=MosekSolver())
 	K=0.2
 	v=6
 	R=20
 	w=0
 	@variable(m, b)

 	epsilon = 0.000000001  #pour avoir une inegalite stricte, on ajoute/retire � b epsilon et on fait une inegalite

	u=0

	#initialisation du terrain

	donnee=loadDataFromFile(instanceName)
	depart=donnee.start
	arrivee=donnee.destination
	gene=donnee.obstacles

	#declaration des coefficients de V(s)
	@variable(m, C[1:10])

	#contrainte sur derivee de V
	#declaration des coefficients de la matrice derivee
	@variable(m, N[1:10 , 1:10] , Symmetric)
	@SDconstraint(m, N <= 0)
	#correspondance entre coefficients par analogie

	@constraint(m, N[1,1] == C[2]*w+C[3]*v +K*C[4]*u)
	@constraint(m, 2*N[1,2] == 2*w*C[5] + C[8]*v + K*u*C[9])
	@constraint(m, 2*N[1,3] == 2*v*C[6]+w*C[8]+K*C[10]*u)
	@constraint(m, 2*N[1,4] == C[9]*w-C[2]*v+C[10]*v+K*(C[7]*u-C[4]))
	@constraint(m, 2*N[1,5] + N[4,4] == -C[9]*v-C[3]*v/2-K*C[7])
	@constraint(m, 2*(N[1,6] + N[4,5]) == v*(C[2]/(3*2)-C[10]/2))
	@constraint(m, 2*N[1,7] + 2*N[4,6] + N[5,5] == v*(C[3]/(4*3*2)+C[9]/(3*2)))
	@constraint(m, 2*N[1,8]+2*N[4,7]+2*N[5,6] == v*(C[10]/(4*3*2)-C[2]/(5*4*3*2)))
	@constraint(m, 2*N[1,9] + 2*N[4,8] +2*N[5,7]+N[6,6] == -v*(C[3]/(6*5*4*3*2)+C[9]/(5*4*3*2)))
	@constraint(m, 2*N[1,10] + 2*N[4,9] + 2*N[5,8] + 2*N[6,7] == v*(C[2]/(7*6*5*4*3*2)-C[10]/(6*5*4*3*2)))
	@constraint(m, 2*N[4,10]+2*N[5,9]+2*N[6,8]+N[7,7] == v*C[9]/(7*6*5*4*3*2))
	@constraint(m, 2*N[2,4] == -2*v*C[5]-K*C[9])
	@constraint(m, 2*2*N[2,5] == -v*C[8])
	@constraint(m, (3*2)*N[2,6] == v*C[5])
	@constraint(m, (4*3*2)*2*N[2,7] == v*C[8])
	@constraint(m, (5*4*3*2)*N[2,8] == -v*C[5])
	@constraint(m, (6*5*4*3*2)*2*N[2,9] == -v*C[8])
	@constraint(m, (7*6*5*4*3*2)*N[2,10] == v*C[5])
	@constraint(m, 2*N[3,4] == -v*C[8]-K*C[10])
	@constraint(m, 2*N[3,5] == -v*C[6])
	@constraint(m, 3*2*2*N[3,6] == v*C[8])
	@constraint(m, 4*3*2*N[3,7] == v*C[6])
	@constraint(m, 5*4*3*2*2*N[3,8] == -v*C[8])
	@constraint(m, 6*5*4*3*2*N[3,9] == -v*C[6])
	@constraint(m, 7*6*5*4*3*2*2*N[3,10] == v*C[8])

	@constraint(m, 2*N[5,10] + 2*N[6,9] + 2*N[7,8] ==0)
	@constraint(m, 2*N[6,10] + 2*N[7,9] + N[8,8] ==0)
	@constraint(m, 2*N[7,10] + 2*N[8,9] ==0)
	@constraint(m, 2*N[8,10] + N[9,9] ==0)

	@constraint(m, N[2,2]==0)
	@constraint(m, N[2,3]==0)
	@constraint(m, N[3,3]==0)
	@constraint(m, N[9,10]==0)
	@constraint(m, N[10,10]==0)

	#contrainte sur le depart
	x_depart=depart[1]
	y_depart=depart[2]
	t_depart=depart[3]
	x_arrivee=arrivee[1]
	y_arrivee=arrivee[2]

 	@constraint(m, C[1]+C[2]*x_depart+C[3]*y_depart+C[4]*t_depart+C[5]*x_depart^2+C[6]*y_depart^2+C[7]*t_depart^2+C[8]*x_depart*y_depart+C[9]*x_depart*t_depart+C[10]*y_depart*t_depart <= b-epsilon)

	#contrainte sur les obstacles
		for i=1:length(gene)
			obs=gene[i]
			if sqrt((obs[1]-x_depart)^2+(obs[2]-y_depart)^2)<=R^2
					obs_matrix=@variable(m, [1:2,1:2], Symmetric)
		 		x_obs = obs[1]
	 			y_obs = obs[2]
				@constraints(m, begin
					obs_matrix[1,1]==C[1]+C[2]*x_obs+C[3]*y_obs+C[5]*x_obs^2+C[6]*y_obs^2+C[8]*x_obs*y_obs-(b+epsilon)
					obs_matrix[1,2]==(C[4]+C[9]*x_obs+C[10]*y_obs)/2
					obs_matrix[2,2]==C[7]
				end)
				@SDconstraint(m, obs_matrix>=0)
			end
	 		i+=1
	 	end

	status=solve(m)

	C1 = getvalue(C)
	print(C1, " \n")
	y1 = linspace(100, 190, 200)
	v1 = Array{Float64}(200)
	for i=1:length(y1)
			v1[i] = C1[1]+C1[2]*50+C1[3]*y1[i]+C1[4]*t_depart+C1[5]*50^2+C1[6]*y1[i]^2+C1[7]*t_depart^2+C1[8]*50*y1[i]+C1[9]*50*t_depart+C1[10]*y1[i]*t_depart
	end
	plot(y1, v1)
	xlabel("X")
	ylabel("V")
	title("Evolution of V between start and end.")
	show()

	n = 100
	y1 = linspace(0, 200, n)
	x1 = linspace(0, 200, n)
	v1 = Array{Float64}(n, n)
	for i=1:length(y1)
		for j=1:length(x1)
			v1[i,j] = C1[1]+C1[2]*x1[j]+C1[3]*y1[i]+C1[4]*t_depart+C1[5]*x1[j]^2+C1[6]*y1[i]^2+C1[7]*t_depart^2+C1[8]*x1[j]*y1[i]+C1[9]*x1[j]*t_depart+C1[10]*y1[i]*t_depart
		end
	end

	xgrid = repmat(x1',n,1)
	ygrid = repmat(y1,1,n)
	fig = figure("pyplot_surfaceplot",figsize=(10,10))
	ax = fig[:add_subplot](1,1,1, projection = "3d")
	ax[:plot_surface](xgrid, ygrid, v1, rstride=2,edgecolors="k", cstride=2, cmap=ColorMap("gray"), alpha=0.2, linewidth=0.25)
	xlabel("X")
	ylabel("Y")
	title("Surface Plot")
	for angle = 1:360
		ax[:view_init](30, angle)
		draw()
		pause(.001)
	end

end



function Q2(instanceName::String)
 	m = Model(solver=MosekSolver())
 	K=0.2
 	v=6
 	R=20
 	w=0
 	@variable(m, b)
 	epsilon = 0.000000001  #pour avoir une inegalite stricte, on ajoute/retire � b epsilon et on fait une inegalite

	u=0

	 #initialisation du terrain

	donnee=loadDataFromFile(instanceName)
	depart=donnee.start
	arrivee=donnee.destination
	gene=donnee.obstacles

	#declaration des coefficients de V(s)
		@variable(m, C[1:10])

#contrainte sur le depart
	x_depart=depart[1]
	y_depart=depart[2]
	t_depart=depart[3]
 	@constraint(m, C[1]+C[2]*x_depart+C[3]*y_depart+C[4]*t_depart+C[5]*x_depart^2+C[6]*y_depart^2+C[7]*t_depart^2+C[8]*x_depart*y_depart+C[9]*x_depart*t_depart+C[10]*y_depart*t_depart <= b-epsilon)

		#contrainte sur les obstacles
			for i=1:length(gene)
				obs=gene[i]
				if sqrt((obs[1]-x_depart)^2+(obs[2]-y_depart)^2)<=R^2
					obs_matrix=@variable(m, [1:2,1:2], Symmetric)
			 		x_obs = obs[1]
		 			y_obs = obs[2]
					@constraints(m, begin
						obs_matrix[1,1]==C[1]+C[2]*x_obs+C[3]*y_obs+C[5]*x_obs^2+C[6]*y_obs^2+C[8]*x_obs*y_obs-(b+epsilon)
						obs_matrix[1,2]==(C[4]+C[9]*x_obs+C[10]*y_obs)/2
						obs_matrix[2,2]==C[7]
					end)

					norme=@variable(m,[1:2])
					@constraints(m,begin
						norme[1]==2*obs_matrix[1,2]
						norme[2]==obs_matrix[1,1]-obs_matrix[2,2]

						obs_matrix[1,1]>=0
						obs_matrix[2,2]>=0
						norm(norme[i] for i=1:2)<=obs_matrix[1,1]+obs_matrix[2,2]
					end)
				end
		 		i+=1
		 	end


 #contrainte sur derivee de V
#declaration des coefficients de la matrice derivee
		@variable(m, N[1:10 , 1:10] , Symmetric)
 			#correspondance entre coefficients par analogie

	@constraint(m, N[1,1] == C[2]*w+C[3]*v +K*C[4]*u)
 	@constraint(m, 2*N[1,2] == 2*w*C[5] + C[8]*v+K*u*C[9])
 	@constraint(m, 2*N[1,3] == 2*v*C[6]+w*C[8]+K*C[10]*u)
 	@constraint(m, 2*N[1,4] == C[9]*w-C[2]*v+C[10]*v+K*(C[7]*u-C[4]))
 	@constraint(m, 2*N[1,5] + N[4,4] == -C[9]*v-C[3]*v/2-K*C[7])
 	@constraint(m, 2*(N[1,6] + N[4,5]) == v*(C[2]/(3*2)-C[10]/2))
 	@constraint(m, 2*N[1,7] + 2*N[4,6] + N[5,5] == v*(C[3]/(4*3*2)+C[9]/(3*2)))
 	@constraint(m, 2*N[1,8]+2*N[4,7]+2*N[5,6] == v*(C[10]/(4*3*2)-C[2]/(5*4*3*2)))
 	@constraint(m, 2*N[1,9] + 2*N[4,8] +2*N[5,7]+N[6,6] == -v*(C[3]/(6*5*4*3*2)+C[9]/(5*4*3*2)))
 	@constraint(m, 2*N[1,10] + 2*N[4,9] + 2*N[5,8] + 2*N[6,7] == v*(C[2]/(7*6*5*4*3*2)-C[10]/(6*5*4*3*2)))
 	@constraint(m, 2*N[4,10]+2*N[5,9]+2*N[6,8]+N[7,7] == v*C[9]/(7*6*5*4*3*2))
 	@constraint(m, 2*N[2,4] == -2*v*C[5]-K*C[9])
 	@constraint(m, 2*2*N[2,5] == -v*C[8])
 	@constraint(m, (3*2)*N[2,6] == v*C[5])
 	@constraint(m, (4*3*2)*2*N[2,7] == v*C[8])
 	@constraint(m, (5*4*3*2)*N[2,8] == -v*C[5])
 	@constraint(m, (6*5*4*3*2)*2*N[2,9] == -v*C[8])
 	@constraint(m, (7*6*5*4*3*2)*N[2,10] == v*C[5])
 	@constraint(m, 2*N[3,4] == -v*C[8]-K*C[10])
 	@constraint(m, 2*N[3,5] == -v*C[6])
 	@constraint(m, 3*2*2*N[3,6] == v*C[8])
 	@constraint(m, 4*3*2*N[3,7] == v*C[6])
 	@constraint(m, 5*4*3*2*2*N[3,8] == -v*C[8])
 	@constraint(m, 6*5*4*3*2*N[3,9] == -v*C[6])
 	@constraint(m, 7*6*5*4*3*2*2*N[3,10] == v*C[8])

 	@constraint(m, 2*N[5,10] + 2*N[6,9] + 2*N[7,8] ==0)
 	@constraint(m, 2*N[6,10] + 2*N[7,9] + N[8,8] ==0)
 	@constraint(m, 2*N[7,10] + 2*N[8,9] ==0)
 	@constraint(m, 2*N[8,10] + N[9,9] ==0)

	@constraint(m, N[2,2]==0)
	@constraint(m, N[2,3]==0)
	@constraint(m, N[3,3]==0)
	@constraint(m, N[9,10]==0)
	@constraint(m, N[10,10]==0)

	for i=1:9
		for j=(i+1):10
			derivee=@variable(m, [1:2,1:2], Symmetric)
			norme=@variable(m,[1:2])
			@constraints(m, begin
				derivee[1,1]==-N[i,i]
				derivee[1,2]==-N[i,j]
				derivee[2,2]==-N[j,j]

				norme[1]==2*derivee[1,2]
				norme[2]==derivee[1,1]-derivee[2,2]

				derivee[1,1]>=0
				derivee[2,2]>=0
				norm(norme[i] for i=1:2)<=derivee[1,1]+derivee[2,2]
			end)

		end
		i+=1
	end

	status=solve(m)
	C1 = getvalue(C)
	print(C1, " \n")
	y1 = linspace(100, 190, 200)
	v1 = Array{Float64}(200)
	for i=1:length(y1)
			v1[i] = C1[1]+C1[2]*50+C1[3]*y1[i]+C1[4]*t_depart+C1[5]*50^2+C1[6]*y1[i]^2+C1[7]*t_depart^2+C1[8]*50*y1[i]+C1[9]*50*t_depart+C1[10]*y1[i]*t_depart
	end
	plot(y1, v1)
	xlabel("X")
	ylabel("V")
	title("Evolution of V between start and end.")
	show()

	n = 100
	y1 = linspace(0, 200, n)
	x1 = linspace(0, 200, n)
	v1 = Array{Float64}(n, n)
	for i=1:length(y1)
		for j=1:length(x1)
			v1[i,j] = C1[1]+C1[2]*x1[j]+C1[3]*y1[i]+C1[4]*t_depart+C1[5]*x1[j]^2+C1[6]*y1[i]^2+C1[7]*t_depart^2+C1[8]*x1[j]*y1[i]+C1[9]*x1[j]*t_depart+C1[10]*y1[i]*t_depart
		end
	end

	xgrid = repmat(x1',n,1)
	ygrid = repmat(y1,1,n)
	fig = figure("pyplot_surfaceplot",figsize=(10,10))
	ax = fig[:add_subplot](1,1,1, projection = "3d")
	ax[:plot_surface](xgrid, ygrid, v1, rstride=2,edgecolors="k", cstride=2, cmap=ColorMap("gray"), alpha=0.2, linewidth=0.25)
	xlabel("X")
	ylabel("Y")
	title("Surface Plot")
	for angle = 1:360
		ax[:view_init](30, angle)
		draw()
		pause(.001)
	end

end




function Q3(instanceName::String)
 	m = Model(solver=MosekSolver())
 	K=0.2
 	v=6
 	R=20
 	w=0
 	@variable(m, b)
 	epsilon = 0.000000001  #pour avoir une inegalite stricte, on ajoute/retire � b epsilon et on fait une inegalite

	u=0

	#initialisation du terrain

	donnee=loadDataFromFile(instanceName)
	depart=donnee.start
	arrivee=donnee.destination
	gene=donnee.obstacles

	#declaration des coefficients de V(s)
	@variable(m, C[1:10])

	#contrainte sur le depart
	x_depart=depart[1]
	y_depart=depart[2]
	t_depart=depart[3]
 	@constraint(m, C[1]+C[2]*x_depart+C[3]*y_depart+C[4]*t_depart+C[5]*x_depart^2+C[6]*y_depart^2+C[7]*t_depart^2+C[8]*x_depart*y_depart+C[9]*x_depart*t_depart+C[10]*y_depart*t_depart <= b-epsilon)

	#contrainte sur les obstacles
		for i=1:length(gene)
			obs=gene[i]
			if sqrt((obs[1]-x_depart)^2+(obs[2]-y_depart)^2)<=R^2
				obst=@variable(m, [1:3])
				absol=@variable(m)
				x_obs = obs[1]
				y_obs = obs[2]
				@constraints(m, begin
					obst[1]==C[1]+C[2]*x_obs+C[3]*y_obs+C[5]*x_obs^2+C[6]*y_obs^2+C[8]*x_obs*y_obs-(b+epsilon)
					obst[2]==(C[4]+C[9]*x_obs+C[10]*y_obs)/2
					obst[3]==C[7]

					absol>=obst[2]
					absol>=-obst[2]

					obst[1]>=absol
					obst[3]>=absol
				end)
			end
			i+=1
		end


	#contrainte sur derivee de V
	#declaration des coefficients de la matrice derivee
	@variable(m, N[1:10 , 1:10] , Symmetric)
	#correspondance entre coefficients par analogie

	@constraint(m, N[1,1] == C[2]*w+C[3]*v +K*C[4]*u)
 	@constraint(m, 2*N[1,2] == 2*w*C[5] + C[8]*v+K*u*C[9])
 	@constraint(m, 2*N[1,3] == 2*v*C[6]+w*C[8]+K*C[10]*u)
 	@constraint(m, 2*N[1,4] == C[9]*w-C[2]*v+C[10]*v+K*(C[7]*u-C[4]))
 	@constraint(m, 2*N[1,5] + N[4,4] == -C[9]*v-C[3]*v/2-K*C[7])
 	@constraint(m, 2*(N[1,6] + N[4,5]) == v*(C[2]/(3*2)-C[10]/2))
 	@constraint(m, 2*N[1,7] + 2*N[4,6] + N[5,5] == v*(C[3]/(4*3*2)+C[9]/(3*2)))
 	@constraint(m, 2*N[1,8]+2*N[4,7]+2*N[5,6] == v*(C[10]/(4*3*2)-C[2]/(5*4*3*2)))
 	@constraint(m, 2*N[1,9] + 2*N[4,8] +2*N[5,7]+N[6,6] == -v*(C[3]/(6*5*4*3*2)+C[9]/(5*4*3*2)))
 	@constraint(m, 2*N[1,10] + 2*N[4,9] + 2*N[5,8] + 2*N[6,7] == v*(C[2]/(7*6*5*4*3*2)-C[10]/(6*5*4*3*2)))
 	@constraint(m, 2*N[4,10]+2*N[5,9]+2*N[6,8]+N[7,7] == v*C[9]/(7*6*5*4*3*2))
 	@constraint(m, 2*N[2,4] == -2*v*C[5]-K*C[9])
 	@constraint(m, 2*2*N[2,5] == -v*C[8])
 	@constraint(m, (3*2)*N[2,6] == v*C[5])
 	@constraint(m, (4*3*2)*2*N[2,7] == v*C[8])
 	@constraint(m, (5*4*3*2)*N[2,8] == -v*C[5])
 	@constraint(m, (6*5*4*3*2)*2*N[2,9] == -v*C[8])
 	@constraint(m, (7*6*5*4*3*2)*N[2,10] == v*C[5])
 	@constraint(m, 2*N[3,4] == -v*C[8]-K*C[10])
 	@constraint(m, 2*N[3,5] == -v*C[6])
 	@constraint(m, 3*2*2*N[3,6] == v*C[8])
 	@constraint(m, 4*3*2*N[3,7] == v*C[6])
 	@constraint(m, 5*4*3*2*2*N[3,8] == -v*C[8])
 	@constraint(m, 6*5*4*3*2*N[3,9] == -v*C[6])
 	@constraint(m, 7*6*5*4*3*2*2*N[3,10] == v*C[8])

 	@constraint(m, 2*N[5,10] + 2*N[6,9] + 2*N[7,8] ==0)
 	@constraint(m, 2*N[6,10] + 2*N[7,9] + N[8,8] ==0)
 	@constraint(m, 2*N[7,10] + 2*N[8,9] ==0)
 	@constraint(m, 2*N[8,10] + N[9,9] ==0)

	@constraint(m, N[2,2]==0)
	@constraint(m, N[2,3]==0)
	@constraint(m, N[3,3]==0)
	@constraint(m, N[9,10]==0)
	@constraint(m, N[10,10]==0)

	for i=1:10
		absneg=@variable(m, [1:10])
		for k=1:10
			@constraint(m, absneg[k]<=N[i,k])
			@constraint(m, absneg[k]<=-N[i,k])
		end
		@constraint(m, 2*N[i,i]<=sum(absneg[j] for j=1:10))
	end

	status=solve(m)

	C1 = getvalue(C)
	print(C1, " \n")
	y1 = linspace(100, 190, 200)
	v1 = Array{Float64}(200)
	for i=1:length(y1)
			v1[i] = C1[1]+C1[2]*50+C1[3]*y1[i]+C1[4]*t_depart+C1[5]*50^2+C1[6]*y1[i]^2+C1[7]*t_depart^2+C1[8]*50*y1[i]+C1[9]*50*t_depart+C1[10]*y1[i]*t_depart
	end
	plot(y1, v1)
	xlabel("y")
	ylabel("V")
	title("Evolution of V between start and end.")
	show()

	n = 100
	y1 = linspace(0, 200, n)
	x1 = linspace(0, 200, n)
	v1 = Array{Float64}(n, n)
	for i=1:length(y1)
		for j=1:length(x1)
			v1[i,j] = C1[1]+C1[2]*x1[j]+C1[3]*y1[i]+C1[4]*t_depart+C1[5]*x1[j]^2+C1[6]*y1[i]^2+C1[7]*t_depart^2+C1[8]*x1[j]*y1[i]+C1[9]*x1[j]*t_depart+C1[10]*y1[i]*t_depart
		end
	end

	xgrid = repmat(x1',n,1)
	ygrid = repmat(y1,1,n)
	fig = figure("pyplot_surfaceplot",figsize=(10,10))
	ax = fig[:add_subplot](1,1,1, projection = "3d")
	ax[:plot_surface](xgrid, ygrid, v1, rstride=2,edgecolors="k", cstride=2, cmap=ColorMap("gray"), alpha=0.2, linewidth=0.25)
	xlabel("X")
	ylabel("Y")
	title("Surface Plot")
	for angle = 1:360
		ax[:view_init](30, angle)
		draw()
		pause(.001)
	end

end





function Q4SDP(instanceName::String)
 	m = Model(solver=MosekSolver())
 	K=0.2
 	v=6
 	R=20
 	w=0
 	@variable(m, b)
 	epsilon = 0.000000001  #pour avoir une inegalite stricte, on ajoute/retire � b epsilon et on fait une inegalite

	u=0

	#initialisation du terrain

	donnee=loadDataFromFile(instanceName)
	depart=donnee.start
	arrivee=donnee.destination
	gene=donnee.obstacles

	#declaration des coefficients de V(s)
	@variable(m, C[1:35])

	#contrainte sur le depart
	x_depart=depart[1]
	y_depart=depart[2]
	t_depart=depart[3]

 	@constraint(m, C[1]+C[2]*x_depart+C[3]*y_depart+C[4]*x_depart^2+C[5]*x_depart^3+C[6]*x_depart^4+C[7]*y_depart^2+C[8]*y_depart^3+C[9]*y_depart^4+ C[10]*x_depart*y_depart+ C[11]*x_depart^2*y_depart + C[12]*x_depart^3*y_depart+ C[13]*x_depart*y_depart^2+ C[14]*x_depart^2*y_depart^2+ C[15]*x_depart*y_depart^3+ C[16]*t_depart+ C[17]*t_depart^2+ C[18]*t_depart^3+ C[19]*t_depart^4+ C[20]*x_depart*t_depart+ C[21]*x_depart^2*t_depart+ C[22]*x_depart^3*t_depart+ C[23]*y_depart*t_depart+ C[24]*y_depart^2*t_depart+ C[25]*y_depart^3*t_depart+ C[26]*x_depart*y_depart*t_depart+ C[27]*x_depart^2*y_depart*t_depart+ C[28]*x_depart*y_depart^2*t_depart+ C[29]*x_depart*t_depart^2+ C[30]*x_depart^2*t_depart^2+ C[31]*y_depart*t_depart^2+ C[32]*y_depart^2*t_depart^2+ C[33]x_depart*y_depart*t_depart^2+ C[34]*x_depart*t_depart^3+C[35]*y_depart*t_depart^3 <= b-epsilon)

	#contrainte sur les obstacles
	for i=1:length(gene)
		obs=gene[i]
		if sqrt((obs[1]-x_depart)^2+(obs[2]-y_depart)^2)<=R^2
				obs_matrix=@variable(m, [1:3,1:3], Symmetric)
			x_obs = obs[1]
			y_obs = obs[2]
			@constraints(m, begin
				obs_matrix[1,1]==C[1]+C[2]*x_obs+C[3]*y_obs+C[4]*x_obs^2+C[5]*x_obs^3+C[6]*x_obs^4+C[7]*y_obs^2+C[8]*y_obs^3+C[9]*y_obs^4+ C[10]*x_obs*y_obs+ C[11]*x_obs^2*y_obs+ C[12]*x_obs^3*y_obs+ C[13]*x_obs*y_obs^2+ C[14]*x_obs^2*y_obs^2+ C[15]*x_obs*y_obs^3-(b+epsilon)
				obs_matrix[1,2]==(C[20]*x_obs+ C[21]*x_obs^2+ C[22]*x_obs^3+ C[23]*y_obs+ C[24]*y_obs^2+ C[25]*y_obs^3+ C[26]*x_obs*y_obs+ C[27]*x_obs^2*y_obs+ C[28]*x_obs*y_obs^2)/2
				2*obs_matrix[1,3]+obs_matrix[2,2] == C[17]+C[29]*x_obs+ C[30]*x_obs^2+ C[31]*y_obs+ C[32]*y_obs^2+ C[33]x_obs*y_obs
				2*obs_matrix[2,3] == C[18]+C[34]*x_obs+C[35]*y_obs
				obs_matrix[3,3] == C[19]
			end)
			@SDconstraint(m, obs_matrix>=0)
		end
		i+=1
	end

 	#contrainte sur derivee de V
	#declaration des coefficients de la matrice derivee
	@variable(m, N[1:8 , 1:8] , Symmetric)
	#correspondance entre coefficients par analogie

	@constraints(m,begin
		N[1,1]==C[6]*v
		2*N[1,2]==v*C[10]
		2*N[1,3]+N[2,2]==v*C[11]
		N[1,4]==v*C[7]
		2*N[1,5]+N[4,4]==3*v*C[8]
		2*N[1,6]==v*C[23]
		2*N[1,7]+N[6,6]==v*C[31]
		N[1,8]+N[2,4]==v*C[13]
		2*N[2,3]==C[12]*v
		2*N[2,5]+2*N[4,8]==v*3*C[15]
		2*N[2,6]==C[26]*v
		2*N[2,7]==C[33]*v
		N[2,8]+N[3,4]==v*C[14]
		N[3,3]==0
		N[3,5]==0
		2*N[3,6]==C[27]*v
		N[3,7]==0
		N[3,8]==0
		2*N[4,5]==v*4*C[9]
		N[4,6]==v*C[24]
		N[4,7]==v*C[32]
		N[5,5]==0
		2*N[5,6]==3*v*C[25]
		N[5,7]==0
		N[5,8]==0
		2*N[6,7]==v*C[35]
		2*N[6,8]==v*2*C[28]
		N[7,7]==0
		N[7,8]==0
		N[8,8]==0
	end)
	@SDconstraint(m,N<=0)
	@variable(m,h)
#	@objective(m, Min, h)
	x_end=arrivee[1]
	y_end=arrivee[2]
	@constraint(m, C[1]+C[2]*x_end+C[3]*y_end+C[4]*x_end^2+C[5]*x_end^3+C[6]*x_end^4+C[7]*y_end^2+C[8]*y_end^3+C[9]*y_end^4 + C[10]*x_end*y_end+C[11]*x_end^2*y_end + C[12]*x_end^3*y_end+C[13]*x_end*y_end^2+C[14]*x_end^2*y_end^2+C[15]*x_end*y_end^3 <= h)
	status=solve(m)
	println(getvalue(C))
end


function Q4SOCP(instanceName::String)
 	m = Model(solver=MosekSolver())
 	K=0.2
 	v=6
 	R=20
 	w=0
 	@variable(m, b)
 	epsilon = 0.000000001  #pour avoir une inegalite stricte, on ajoute/retire � b epsilon et on fait une inegalite

	u=0

	#initialisation du terrain

	donnee=loadDataFromFile(instanceName)
	depart=donnee.start
	arrivee=donnee.destination
	gene=donnee.obstacles

	#declaration des coefficients de V(s)
	@variable(m, C[1:35])

	#contrainte sur le depart
	x_depart=depart[1]
	y_depart=depart[2]
	t_depart=depart[3]

 	@constraint(m, C[1]+C[2]*x_depart+C[3]*y_depart+C[4]*x_depart^2+C[5]*x_depart^3+C[6]*x_depart^4+C[7]*y_depart^2+C[8]*y_depart^3+C[9]*y_depart^4+ C[10]*x_depart*y_depart+ C[11]*x_depart^2*y_depart + C[12]*x_depart^3*y_depart+ C[13]*x_depart*y_depart^2+ C[14]*x_depart^2*y_depart^2+ C[15]*x_depart*y_depart^3+ C[16]*t_depart+ C[17]*t_depart^2+ C[18]*t_depart^3+ C[19]*t_depart^4+ C[20]*x_depart*t_depart+ C[21]*x_depart^2*t_depart+ C[22]*x_depart^3*t_depart+ C[23]*y_depart*t_depart+ C[24]*y_depart^2*t_depart+ C[25]*y_depart^3*t_depart+ C[26]*x_depart*y_depart*t_depart+ C[27]*x_depart^2*y_depart*t_depart+ C[28]*x_depart*y_depart^2*t_depart+ C[29]*x_depart*t_depart^2+ C[30]*x_depart^2*t_depart^2+ C[31]*y_depart*t_depart^2+ C[32]*y_depart^2*t_depart^2+ C[33]x_depart*y_depart*t_depart^2+ C[34]*x_depart*t_depart^3+C[35]*y_depart*t_depart^3 <= b-epsilon)

	#contrainte sur les obstacles
	for i=1:length(gene)
		obs=gene[i]
		if sqrt((obs[1]-x_depart)^2+(obs[2]-y_depart)^2)<=R^2
				obs_matrix=@variable(m, [1:3,1:3], Symmetric)
			x_obs = obs[1]
			y_obs = obs[2]
			@constraints(m, begin
				obs_matrix[1,1]==C[1]+C[2]*x_obs+C[3]*y_obs+C[4]*x_obs^2+C[5]*x_obs^3+C[6]*x_obs^4+C[7]*y_obs^2+C[8]*y_obs^3+C[9]*y_obs^4+ C[10]*x_obs*y_obs+ C[11]*x_obs^2*y_obs+ C[12]*x_obs^3*y_obs+ C[13]*x_obs*y_obs^2+ C[14]*x_obs^2*y_obs^2+ C[15]*x_obs*y_obs^3-(b+epsilon)
				obs_matrix[1,2]==(C[20]*x_obs+ C[21]*x_obs^2+ C[22]*x_obs^3+ C[23]*y_obs+ C[24]*y_obs^2+ C[25]*y_obs^3+ C[26]*x_obs*y_obs+ C[27]*x_obs^2*y_obs+ C[28]*x_obs*y_obs^2)/2
				2*obs_matrix[1,3]+obs_matrix[2,2] == C[17]+C[29]*x_obs+ C[30]*x_obs^2+ C[31]*y_obs+ C[32]*y_obs^2+ C[33]x_obs*y_obs
				2*obs_matrix[2,3] == C[18]+C[34]*x_obs+C[35]*y_obs
				obs_matrix[3,3] == C[19]
			end)
			for j=1:2
				for k=i+1:3

					norme=@variable(m,[1:2])
					@constraints(m,begin
						norme[1]==2*obs_matrix[j,k]
						norme[2]==obs_matrix[j,j]-obs_matrix[k,k]

						obs_matrix[j,j]>=0
						obs_matrix[k,k]>=0
						norm(norme[l] for l=1:2)<=obs_matrix[j,j]+obs_matrix[k,k]
					end)
				end
			end
		end
		i+=1
	end

	#contrainte sur derivee de V
	#declaration des coefficients de la matrice derivee
	@variable(m, N[1:8 , 1:8] , Symmetric)
	#correspondance entre coefficients par analogie

	@constraints(m,begin
		N[1,1]==C[6]*v
		2*N[1,2]==v*C[10]
		2*N[1,3]+N[2,2]==v*C[11]
		N[1,4]==v*C[7]
		2*N[1,5]+N[4,4]==3*v*C[8]
		2*N[1,6]==v*C[23]
		2*N[1,7]+N[6,6]==v*C[31]
		N[1,8]+N[2,4]==v*C[13]
		2*N[2,3]==C[12]*v
		2*N[2,5]+2*N[4,8]==v*3*C[15]
		2*N[2,6]==C[26]*v
		2*N[2,7]==C[33]*v
		N[2,8]+N[3,4]==v*C[14]
		N[3,3]==0
		N[3,5]==0
		2*N[3,6]==C[27]*v
		N[3,7]==0
		N[3,8]==0
		2*N[4,5]==v*4*C[9]
		N[4,6]==v*C[24]
		N[4,7]==v*C[32]
		N[5,5]==0
		2*N[5,6]==3*v*C[25]
		N[5,7]==0
		N[5,8]==0
		2*N[6,7]==v*C[35]
		2*N[6,8]==v*2*C[28]
		N[7,7]==0
		N[7,8]==0
		N[8,8]==0
	end)

	for i=1:7
		for j=i+1:8
			derivee=@variable(m,[1:2,1:2], Symmetric)
			@constraints(m, begin
				derivee[1,1]==-N[i,i]
				derivee[1,2]==-N[i,j]
				derivee[2,2]==-N[j,j]
			end)
			norme=@variable(m,[1:2])
			@constraints(m,begin
				norme[1]==2*derivee[1,2]
				norme[2]==derivee[1,1]-derivee[2,2]

				derivee[1,1]>=0
				derivee[2,2]>=0
				norm(norme[l] for l=1:2)<=derivee[1,1]+derivee[2,2]
			end)
		end
	end


	@variable(m,h)
#	@objective(m, Min, h)
	x_end=arrivee[1]
	y_end=arrivee[2]
	@constraint(m, C[1]+C[2]*x_end+C[3]*y_end+C[4]*x_end^2+C[5]*x_end^3+C[6]*x_end^4+C[7]*y_end^2+C[8]*y_end^3+C[9]*y_end^4 + C[10]*x_end*y_end+C[11]*x_end^2*y_end + C[12]*x_end^3*y_end+C[13]*x_end*y_end^2+C[14]*x_end^2*y_end^2+C[15]*x_end*y_end^3 <= h)

	status=solve(m)
	println(getvalue(C))
end


function Q4LP(instanceName::String)
 	m = Model(solver=MosekSolver())
 	K=0.2
 	v=6
 	R=20
 	w=0
 	@variable(m, b)
 	epsilon = 0.000000001  #pour avoir une inegalite stricte, on ajoute/retire � b epsilon et on fait une inegalite

	u=0

	#initialisation du terrain

	donnee=loadDataFromFile(instanceName)
	depart=donnee.start
	arrivee=donnee.destination
	gene=donnee.obstacles

	#declaration des coefficients de V(s)
	@variable(m, C[1:35])

	#contrainte sur le depart
	x_depart=depart[1]
	y_depart=depart[2]
	t_depart=depart[3]

 	@constraint(m, C[1]+C[2]*x_depart+C[3]*y_depart+C[4]*x_depart^2+C[5]*x_depart^3+C[6]*x_depart^4+C[7]*y_depart^2+C[8]*y_depart^3+C[9]*y_depart^4+ C[10]*x_depart*y_depart+ C[11]*x_depart^2*y_depart + C[12]*x_depart^3*y_depart+ C[13]*x_depart*y_depart^2+ C[14]*x_depart^2*y_depart^2+ C[15]*x_depart*y_depart^3+ C[16]*t_depart+ C[17]*t_depart^2+ C[18]*t_depart^3+ C[19]*t_depart^4+ C[20]*x_depart*t_depart+ C[21]*x_depart^2*t_depart+ C[22]*x_depart^3*t_depart+ C[23]*y_depart*t_depart+ C[24]*y_depart^2*t_depart+ C[25]*y_depart^3*t_depart+ C[26]*x_depart*y_depart*t_depart+ C[27]*x_depart^2*y_depart*t_depart+ C[28]*x_depart*y_depart^2*t_depart+ C[29]*x_depart*t_depart^2+ C[30]*x_depart^2*t_depart^2+ C[31]*y_depart*t_depart^2+ C[32]*y_depart^2*t_depart^2+ C[33]x_depart*y_depart*t_depart^2+ C[34]*x_depart*t_depart^3+C[35]*y_depart*t_depart^3 <= b-epsilon)

	#contrainte sur les obstacles
	for i=1:length(gene)
		obs=gene[i]
		if sqrt((obs[1]-x_depart)^2+(obs[2]-y_depart)^2)<=R^2
				obs_matrix=@variable(m, [1:3,1:3], Symmetric)
			x_obs = obs[1]
			y_obs = obs[2]
			@constraints(m, begin
				obs_matrix[1,1]==C[1]+C[2]*x_obs+C[3]*y_obs+C[4]*x_obs^2+C[5]*x_obs^3+C[6]*x_obs^4+C[7]*y_obs^2+C[8]*y_obs^3+C[9]*y_obs^4+ C[10]*x_obs*y_obs+ C[11]*x_obs^2*y_obs+ C[12]*x_obs^3*y_obs+ C[13]*x_obs*y_obs^2+ C[14]*x_obs^2*y_obs^2+ C[15]*x_obs*y_obs^3-(b+epsilon)
				obs_matrix[1,2]==(C[20]*x_obs+ C[21]*x_obs^2+ C[22]*x_obs^3+ C[23]*y_obs+ C[24]*y_obs^2+ C[25]*y_obs^3+ C[26]*x_obs*y_obs+ C[27]*x_obs^2*y_obs+ C[28]*x_obs*y_obs^2)/2
				2*obs_matrix[1,3]+obs_matrix[2,2] == C[17]+C[29]*x_obs+ C[30]*x_obs^2+ C[31]*y_obs+ C[32]*y_obs^2+ C[33]x_obs*y_obs
				2*obs_matrix[2,3] == C[18]+C[34]*x_obs+C[35]*y_obs
				obs_matrix[3,3] == C[19]
			end)

			for j=1:3
				absneg=@variable(m, [1:3])
				for k=1:3
					@constraint(m, absneg[k]<=obs_matrix[j,k])
					@constraint(m, absneg[k]<=-obs_matrix[j,k])
				end
				@constraint(m, 2*obs_matrix[i,i]<=sum(absneg[j] for j=1:3))
			end
		end
		i+=1
	end




	#contrainte sur derivee de V
	#declaration des coefficients de la matrice derivee
	@variable(m, N[1:8 , 1:8] , Symmetric)
	#correspondance entre coefficients par analogie

	@constraints(m,begin
		N[1,1]==C[6]*v
		2*N[1,2]==v*C[10]
		2*N[1,3]+N[2,2]==v*C[11]
		N[1,4]==v*C[7]
		2*N[1,5]+N[4,4]==3*v*C[8]
		2*N[1,6]==v*C[23]
		2*N[1,7]+N[6,6]==v*C[31]
		N[1,8]+N[2,4]==v*C[13]
		2*N[2,3]==C[12]*v
		2*N[2,5]+2*N[4,8]==v*3*C[15]
		2*N[2,6]==C[26]*v
		2*N[2,7]==C[33]*v
		N[2,8]+N[3,4]==v*C[14]
		N[3,3]==0
		N[3,5]==0
		2*N[3,6]==C[27]*v
		N[3,7]==0
		N[3,8]==0
		2*N[4,5]==v*4*C[9]
		N[4,6]==v*C[24]
		N[4,7]==v*C[32]
		N[5,5]==0
		2*N[5,6]==3*v*C[25]
		N[5,7]==0
		N[5,8]==0
		2*N[6,7]==v*C[35]
		2*N[6,8]==v*2*C[28]
		N[7,7]==0
		N[7,8]==0
		N[8,8]==0
	end)

	for i=1:8
		absneg=@variable(m, [1:8])
			for k=1:8
				@constraint(m, absneg[k]<=N[i,k])
				@constraint(m, absneg[k]<=-N[i,k])
			end
		@constraint(m, 2*N[i,i]<=sum(absneg[j] for j=1:8))
	end


	@variable(m,h)
#	@objective(m, Min, h)
	x_end=arrivee[1]
	y_end=arrivee[2]
	@constraint(m, C[1]+C[2]*x_end+C[3]*y_end+C[4]*x_end^2+C[5]*x_end^3+C[6]*x_end^4+C[7]*y_end^2+C[8]*y_end^3+C[9]*y_end^4 + C[10]*x_end*y_end+C[11]*x_end^2*y_end + C[12]*x_end^3*y_end+C[13]*x_end*y_end^2+C[14]*x_end^2*y_end^2+C[15]*x_end*y_end^3 <= h)

	status=solve(m)
println(getvalue(C))
end


function Q5(instanceName::String)
 	m = Model(solver=MosekSolver())
 	K=0.2
 	v=6
 	R=20
 	w=0
 	@variable(m, b)

 	epsilon = 0.000000001  #pour avoir une inegalite stricte, on ajoute/retire � b epsilon et on fait une inegalite

	u=3*pi/10

	#initialisation du terrain

	donnee=loadDataFromFile(instanceName)
	depart=donnee.start
	arrivee=donnee.destination
	gene=donnee.obstacles

	#declaration des coefficients de V(s)
	@variable(m, C[1:10])

	#contrainte sur derivee de V
	#declaration des coefficients de la matrice derivee
	@variable(m, N[1:10 , 1:10] , Symmetric)
	@SDconstraint(m, N <= 0)
	#correspondance entre coefficients par analogie

	@constraint(m, N[1,1] == C[2]*w+C[3]*v +K*C[4]*u)
	@constraint(m, 2*N[1,2] == 2*w*C[5] + C[8]*v+K*u*C[9])
	@constraint(m, 2*N[1,3] == 2*v*C[6]+w*C[8]+K*C[10]*u)
	@constraint(m, 2*N[1,4] == C[9]*w-C[2]*v+C[10]*v+K*(C[7]*u-C[4]))
	@constraint(m, 2*N[1,5] + N[4,4] == -C[9]*v-C[3]*v/2-K*C[7])
	@constraint(m, 2*(N[1,6] + N[4,5]) == v*(C[2]/(3*2)-C[10]/2))
	@constraint(m, 2*N[1,7] + 2*N[4,6] + N[5,5] == v*(C[3]/(4*3*2)+C[9]/(3*2)))
	@constraint(m, 2*N[1,8]+2*N[4,7]+2*N[5,6] == v*(C[10]/(4*3*2)-C[2]/(5*4*3*2)))
	@constraint(m, 2*N[1,9] + 2*N[4,8] +2*N[5,7]+N[6,6] == -v*(C[3]/(6*5*4*3*2)+C[9]/(5*4*3*2)))
	@constraint(m, 2*N[1,10] + 2*N[4,9] + 2*N[5,8] + 2*N[6,7] == v*(C[2]/(7*6*5*4*3*2)-C[10]/(6*5*4*3*2)))
	@constraint(m, 2*N[4,10]+2*N[5,9]+2*N[6,8]+N[7,7] == v*C[9]/(7*6*5*4*3*2))
	@constraint(m, 2*N[2,4] == -2*v*C[5]-K*C[9])
	@constraint(m, 2*2*N[2,5] == -v*C[8])
	@constraint(m, (3*2)*N[2,6] == v*C[5])
	@constraint(m, (4*3*2)*2*N[2,7] == v*C[8])
	@constraint(m, (5*4*3*2)*N[2,8] == -v*C[5])
	@constraint(m, (6*5*4*3*2)*2*N[2,9] == -v*C[8])
	@constraint(m, (7*6*5*4*3*2)*N[2,10] == v*C[5])
	@constraint(m, 2*N[3,4] == -v*C[8]-K*C[10])
	@constraint(m, 2*N[3,5] == -v*C[6])
	@constraint(m, 3*2*2*N[3,6] == v*C[8])
	@constraint(m, 4*3*2*N[3,7] == v*C[6])
	@constraint(m, 5*4*3*2*2*N[3,8] == -v*C[8])
	@constraint(m, 6*5*4*3*2*N[3,9] == -v*C[6])
	@constraint(m, 7*6*5*4*3*2*2*N[3,10] == v*C[8])

	@constraint(m, 2*N[5,10] + 2*N[6,9] + 2*N[7,8] ==0)
	@constraint(m, 2*N[6,10] + 2*N[7,9] + N[8,8] ==0)
	@constraint(m, 2*N[7,10] + 2*N[8,9] ==0)
	@constraint(m, 2*N[8,10] + N[9,9] ==0)

	@constraint(m, N[2,2]==0)
	@constraint(m, N[2,3]==0)
	@constraint(m, N[3,3]==0)
	@constraint(m, N[9,10]==0)
	@constraint(m, N[10,10]==0)

	#contrainte sur le depart
	x_depart=depart[1]
	y_depart=depart[2]
	t_depart=depart[3]
	x_arrivee=arrivee[1]
	y_arrivee=arrivee[2]

 	@constraint(m, C[1]+C[2]*x_depart+C[3]*y_depart+C[4]*t_depart+C[5]*x_depart^2+C[6]*y_depart^2+C[7]*t_depart^2+C[8]*x_depart*y_depart+C[9]*x_depart*t_depart+C[10]*y_depart*t_depart <= b-epsilon)

	#contrainte sur les obstacles
	for i=1:length(gene)
		obs=gene[i]
		if sqrt((obs[1]-x_depart)^2+(obs[2]-y_depart)^2)<=R^2
				obs_matrix=@variable(m, [1:2,1:2], Symmetric)
			x_obs = obs[1]
			y_obs = obs[2]
			@constraints(m, begin
				obs_matrix[1,1]==C[1]+C[2]*x_obs+C[3]*y_obs+C[5]*x_obs^2+C[6]*y_obs^2+C[8]*x_obs*y_obs-(b+epsilon)
				obs_matrix[1,2]==(C[4]+C[9]*x_obs+C[10]*y_obs)/2
				obs_matrix[2,2]==C[7]
			end)
			@SDconstraint(m, obs_matrix>=0)
		end
		i+=1
	end

	status=solve(m)
	print(status)


	C1 = getvalue(C)
	print(C1, " \n")
	n = 100
	y1 = Array{Float64}(n)
	x1 = Array{Float64}(n)
	t1 = Array{Float64}(n)
	v1 = Array{Float64}(n)
	delta=1
	#print(x_depart, " ", y_depart, " \n")
	y1[1]=y_depart
	x1[1]=x_depart
	t1[1]=t_depart
	i=1
	v1[i] = C1[1]+C1[2]*x1[i]+C1[3]*y1[i]+C1[4]*t1[i]+C1[5]*x1[i]^2+C1[6]*y1[i]^2+C1[7]*t1[i]^2+C1[8]*x1[i]*y1[i]+C1[9]*x1[i]*t1[i]+C1[10]*y1[i]*t1[i]


	for i=1:n-1
		e = Model(solver=MosekSolver(MSK_IPAR_LOG=0))
		@variable(e, dir)

		xdot=-v*sin(t1[i])+w
		ydot=v*cos(t1[i])
		@variable(e,tdot)
		@constraint(e, tdot==-K*(t1[i]-dir))

		@variable(e, absdir)
		@constraint(e, absdir >= dir-u)
		@constraint(e, absdir >= -dir+u)

		vdot= C1[2]*xdot+C1[3]*ydot+C1[4]*tdot+2*C1[5]*x1[i]*xdot+2*C1[6]*y1[i]*ydot+2*C1[7]*t1[i]*tdot+C1[8]*x1[i]*ydot+C1[8]*y1[i]*xdot+C1[9]*x1[i]*tdot+C1[9]*t1[i]*xdot+C1[10]*y1[i]*tdot+C1[10]*t1[i]*ydot
		@constraint(e, vdot<=0)
		@objective(e, Min, absdir)
		status=solve(e)

		direction=getvalue(dir)

		y1[i+1]=y1[i]+delta*v*cos(t1[i])
		x1[i+1]=x1[i]+delta*(-v*sin(t1[i])+w)
		t1[i+1]=t1[i]+delta*(-K*(t1[i]-direction))
		v1[i+1]= C1[1]+C1[2]*x1[i+1]+C1[3]*y1[i+1]+C1[4]*t1[i+1]+C1[5]*x1[i+1]^2+C1[6]*y1[i+1]^2+C1[7]*t1[i+1]^2+C1[8]*x1[i+1]*y1[i+1]+C1[9]*x1[i+1]*t1[i+1]+C1[10]*y1[i+1]*t1[i+1]

	end
	#print(x1, " \n", y1, " \n")
	println(t1[length(t1)]-3*pi/10)

	fig = figure("pyplot_surfaceplot",figsize=(10,10))
	ax = fig[:add_subplot](1,1,1, projection = "3d")
	ax[:plot3D](x1, y1, v1)
	xlabel("X")
	ylabel("Y")
	title("3D Plot")
	for angle = 1:360
		ax[:view_init](30, angle)
		draw()
		pause(.001)
	end
println(x1)
println(y1)
println(getvalue(C))

end


function Q7(instanceName::String)
	K=0.2
 	v=6
 	R=20
	w=0

	donnee=generateData(5)
	depart=donnee.start
	println(depart)
	t = depart[3]
	arrivee=donnee.destination
	println(arrivee)
	obs = Array{Tuple{Float64, Float64}}(0)
	non_vu = donnee.obstacles
	pos_list = Array{Tuple{Float64, Float64, Float64}}(0)
	pos_list = push!(pos_list, depart)

	pos = depart
	obs, non_vu = find_new_obs(obs, non_vu, depart[1], depart[2], R)
	print(obs, " \n", non_vu)
	good_pos=depart
	tolerance=0.05
	n = 10
	u = linspace(-pi, pi, n)
	C = Array{Float64}(n, 10)

	bon_u = Array{Float64}(0)
	while get_dist(pos, arrivee) > tolerance
		obs, non_vu = find_new_obs(obs, non_vu, pos[1], pos[2], R)
		remove_unwanted(obs, pos, arrivee)
		temp=min(get_dist(pos, arrivee)/v/5, R/v*3/4)
		for (index, value) in enumerate(u)
			C, optimal = solving(instanceName, K, v, R, w, value, depart, arrivee, obs)
			if optimal
				bon_u = push!(bon_u, value)
			end
		end
		distance = sqrt((pos[1] - arrivee[1])^2 + (pos[2] - arrivee[2])^2)
	#	good_pos = 0
		for (index, value) in enumerate(bon_u)
			next_pos = compute_position(pos[1], pos[2], pos[3], value, v, w, K, temp, arrivee, tolerance, C)
			tmp = sqrt((next_pos[1] - arrivee[1])^2 + (next_pos[2] - arrivee[2])^2)
			println(distance," ", tmp)
			if tmp < distance
				distance = tmp
				good_pos = next_pos
			end
		end
		empty!(bon_u)
		pos = good_pos
		pos_list = push!(pos_list, pos)
		println(pos, temp)
		if length(pos_list) >2 && pos_list[length(pos_list)]==pos_list[length(pos_list)-1]
			break
		end
	end

	x = Array{Float64}(length(pos_list))
	y = Array{Float64}(length(pos_list))
	for i=1:length(pos_list)
		x[i] = pos_list[i][1]
		y[i] = pos_list[i][2]
	end

#	scatter(x, y)

	x = Array{Float64}(length(donnee.obstacles))
	y = Array{Float64}(length(donnee.obstacles))
	for i=1:length(donnee.obstacles)
		x[i] = donnee.obstacles[i][1]
		y[i] = donnee.obstacles[i][2]
	end
#	scatter(x, y)
#	show()
end
