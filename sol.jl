using JuMP
using PyPlot
using Mosek
include("data.jl")

function solving(instanceName::String, K, v, R, w, u, depart, arrivee, gene)
    # Cette fonction utilise le modèle implémenté pour la question 1 et retourne les coéficient de C
    m = Model(solver=MosekSolver(MSK_IPAR_LOG=0))
    @variable(m, b)

    epsilon = 0.000000001  #pour avoir une inegalite stricte, on ajoute/retire � b epsilon et on fait une inegalite

   #initialisation du terrain

   #declaration des coefficients de V(s)
   @variable(m, C[1:10])

   #contrainte sur derivee de V
   #declaration des coefficients de la matrice derivee
   @variable(m, N[1:10 , 1:10] , Symmetric)
   @SDconstraint(m, N <= 0)
   #correspondance entre coefficients par analogie

   @constraint(m, N[1,1] == C[2]*w+C[3]*v +K*C[4]*u)
   @constraint(m, 2*N[1,2] == 2*w*C[5] + C[3]*v+K*u*C[9])
   @constraint(m, 2*N[1,3] == 2*v*C[6]+w*C[8]+K*C[10]*u)
   @constraint(m, 2*N[1,4] == C[9]*w-C[2]*v+C[10]*v+K*(C[7]*u-C[4]))
   @constraint(m, 2*N[1,5] + N[4,4] == -C[9]*v-C[10]*v/2-K*C[7])
   @constraint(m, 2*(N[1,6] + N[4,5]) == v*(C[2]/(3*2)-C[10]/2))
   @constraint(m, 2*N[1,7] + 2*N[4,6] + N[5,5] == v*(C[3]/(4*3*2)+C[9]/(3*2)))
   @constraint(m, 2*N[1,8]+2*N[4,7]+2*N[5,6] == v*(C[10]/(4*3*2)-C[2]/(5*4*3*2)))
   @constraint(m, 2*N[1,9] + 2*N[4,8] +2*N[5,7]+N[6,6] == -v*(C[3]/(6*5*4*3*2)+C[9]/(5*4*3*2)))
   @constraint(m, 2*N[1,10] + 2*N[4,9] + 2*N[5,8] + 2*N[6,7] == v*(C[2]/(7*6*5*4*3*2)-C[10]/(6*5*4*3*2)))
   @constraint(m, 2*N[4,10]+2*N[5,9]+2*N[6,8]+N[7,7] == v*C[9]/(7*6*5*4*3*2))
   @constraint(m, 2*N[2,4] == -2*v*C[5]-K*C[9])
   @constraint(m, 2*2*N[2,5] == -v*C[8])
   @constraint(m, (3*2)*N[2,6] == v*C[5])
   @constraint(m, (4*3*2)*2*N[2,7] == v*C[8])
   @constraint(m, (5*4*3*2)*N[2,8] == -v*C[5])
   @constraint(m, (6*5*4*3*2)*2*N[2,9] == -v*C[8])
   @constraint(m, (7*6*5*4*3*2)*N[2,10] == v*C[5])
   @constraint(m, 2*N[3,4] == -v*C[8]-K*C[10])
   @constraint(m, 2*N[3,5] == -v*C[6])
   @constraint(m, 3*2*2*N[3,6] == v*C[8])
   @constraint(m, 4*3*2*N[3,7] == v*C[6])
   @constraint(m, 5*4*3*2*2*N[3,8] == -v*C[8])
   @constraint(m, 6*5*4*3*2*N[3,9] == -v*C[6])
   @constraint(m, 7*6*5*4*3*2*2*N[3,10] == v*C[8])

   @constraint(m, 2*N[5,10] + 2*N[6,9] + 2*N[7,8] ==0)
   @constraint(m, 2*N[6,10] + 2*N[7,9] + N[8,8] ==0)
   @constraint(m, 2*N[7,10] + 2*N[8,9] ==0)
   @constraint(m, 2*N[8,10] + N[9,9] ==0)

   @constraint(m, N[2,2]==0)
   @constraint(m, N[2,3]==0)
   @constraint(m, N[3,3]==0)
   @constraint(m, N[9,10]==0)
   @constraint(m, N[10,10]==0)

   #contrainte sur le depart
   x_depart=depart[1]
   y_depart=depart[2]
   t_depart=depart[3]
   x_arrivee=arrivee[1]
   y_arrivee=arrivee[2]

    @constraint(m, C[1]+C[2]*x_depart+C[3]*y_depart+C[4]*t_depart+C[5]*x_depart^2+C[6]*y_depart^2+C[7]*t_depart^2+C[8]*x_depart*y_depart+C[9]*x_depart*t_depart+C[10]*y_depart*t_depart <= b-epsilon)

    #contrainte sur l'arrivee

    @variable(m, arriv[1:2,1:2], Symmetric)
    @constraints(m, begin
        arriv[1,1]==C[1]+C[2]*x_arrivee+C[3]*y_arrivee+C[5]*x_arrivee^2+C[6]*y_arrivee^2+C[8]*x_arrivee*y_arrivee-(b-epsilon)
        arriv[1,2]==(C[4]+C[9]*x_arrivee+C[10]*y_arrivee)/2
        arriv[2,2]==C[7]
    end)
    @SDconstraint(m, arriv<=0)

       #contrainte sur les obstacles
       for i=1:length(gene)
           obs=gene[i]
               obs_matrix=@variable(m, [1:2,1:2], Symmetric)
                x_obs = obs[1]
                y_obs = obs[2]
               @constraints(m, begin
                   obs_matrix[1,1]==C[1]+C[2]*x_obs+C[3]*y_obs+C[5]*x_obs^2+C[6]*y_obs^2+C[8]*x_obs*y_obs-(b+epsilon)
                   obs_matrix[1,2]==(C[4]+C[9]*x_obs+C[10]*y_obs)/2
                   obs_matrix[2,2]==C[7]
               end)
               @SDconstraint(m, obs_matrix>=0)
            i+=1
        end

   status=solve(m)
   return getvalue(C), status == :Optimal
end

function compute_v(x, y, t1, C1)
    return C1[1]+C1[2]*x+C1[3]*y+C1[4]*t1+C1[5]*x^2+C1[6]*y^2+C1[7]*t1^2+C1[8]*x*y+C1[9]*x*t1+C1[10]*y*t1
end

function get_dist(start, finish)
    return sqrt((start[1]-finish[1])^2+(start[2]-finish[2])^2)
end

function compute_position(x_depart, y_depart, t, u, v, w, K, temps, arrivee, tolerance, C1)
    # Calcul la position du drone avec les valeurs de départ après un certain temps
    y1 = Array{Float64}(1)
	x1 = Array{Float64}(1)
	t1 = Array{Float64}(1)
    delta=0.01
    tmp = delta
	y1[1]=y_depart
	x1[1]=x_depart
    t1[1]=t
    i = 1
    dist= sqrt((x1[i] - arrivee[1])^2 + (y1[i] - arrivee[2])^2)
	while tmp < temps && dist > tolerance

        e = Model(solver=MosekSolver(MSK_IPAR_LOG=0))
        @variable(e, dir)

        xdot=-v*sin(t1[i])+w
        ydot=v*cos(t1[i])
        tdot=-K*(t1[i]-u)
        @variable(e,tdot)
        @constraint(e, tdot==-K*(t1[i]-dir))

        @variable(e, absdir)
        @constraint(e, absdir >= dir-u)
        @constraint(e, absdir >= -dir+u)

        vdot= C1[2]*xdot+C1[3]*ydot+C1[4]*tdot+2*C1[5]*x1[i]*xdot+2*C1[6]*y1[i]*ydot+2*C1[7]*t1[i]*tdot+C1[8]*x1[i]*ydot+C1[8]*y1[i]*xdot+C1[9]*x1[i]*tdot+C1[9]*t1[i]*xdot+C1[10]*y1[i]*tdot+C1[10]*t1[i]*ydot
        @constraint(e, vdot<=0)
        @objective(e, Min, absdir)
        status=solve(e)

        direction=getvalue(dir)
        tdot=-K*(t1[i]-direction)
        vdot=C1[2]*xdot+C1[3]*ydot+C1[4]*tdot+2*C1[5]*x1[i]*xdot+2*C1[6]*y1[i]*ydot+2*C1[7]*t1[i]*tdot+C1[8]*x1[i]*ydot+C1[8]*y1[i]*xdot+C1[9]*x1[i]*tdot+C1[9]*t1[i]*xdot+C1[10]*y1[i]*tdot+C1[10]*t1[i]*ydot
        if vdot>0
            println("there")
            break
        end
		push!(y1, y1[i]+delta*ydot)
        push!(x1, x1[i]+delta*xdot)
        push!(t1, t1[i]+delta*(-K*(t1[i]-u)))
        tmp = tmp + delta
        i += 1
    end
    println(tmp, ' ', sqrt((x1[i] - arrivee[1])^2 + (y1[i] - arrivee[2])^2))
    return x1[length(x1)], y1[length(y1)], t1[length(t1)]
end

function find_new_obs(obs, non_vu, x_depart, y_depart, R)

    i = 1
    while i < length(non_vu)
        if sqrt((non_vu[i][1] - x_depart)^2 + (non_vu[i][2] - y_depart)^2) <= R^2
            push!(obs, non_vu[i])
            deleteat!(non_vu, i)
        end
        i += 1
    end
    return obs, non_vu
end

function remove_unwanted(obs, depart, arrivee)

    i=length(obs)
    while i>0
        if abs(getsign(arrivee[1]-depart[1]) - getsign(obs[i][1]-depart[1])) > 1
            if abs(getsign(arrivee[2]-depart[2]) - getsign(obs[i][2]-depart[2])) >1
                deleteat!(obs, i)
            end
        end
        i-=1
    end
end

function getsign(value)
    if value < 0.0
        return -1
    elseif value==0.0
        return 0
    end
    return 1
end
